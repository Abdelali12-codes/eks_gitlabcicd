# kubectl rollout
1. history 
```
kubectl rollout history deployment/name 
```
2. undo 

```
kubectl rollout undo deployment/name 
```
3. 

```
kubectl rollout undo deployment/name --to-revision=2
```
4. restart
```
kubectl rollout restart deployment/name
```

# References
* https://docs.gitlab.com/ee/ci/yaml/#dependencies